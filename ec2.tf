
## VARIABLES
#######################################################

variable "shared_credentials_file" {
  default = ".aws/credentials"
}

variable "region" {
  default = "eu-west-2"
}

variable "profile" {
  default = "terraform"
}

## PROVIDER
#######################################################

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.shared_credentials_file}"
  profile                 = "${var.profile}"
}

## RESOURCES
#######################################################
# Our default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "terras" {
  name        = "terraform_example"
  description = "Used in the terraform"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["52.56.113.187/32"]
  }

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["62.232.124.46/32"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_instance" "web" {
  ami = "ami-07dc734dc14746eab"
  instance_type = "t2.micro"
  key_name = "terraform"
  vpc_security_group_ids = ["${aws_security_group.terras.id}"]
  tags {
    Name = "terraform_jemnkins"
  }
  #user_data = "${file("chef_installer.sh")}"

  provisioner "chef" {

    connection {
      type     = "ssh"
      user     = "ubuntu"
      private_key = "${file("terraform.pem")}"
      }
    environment     = "_default"
    run_list        = ["hardened_node"]
    node_name       = "timmyterra"
    server_url      = "https://api.chef.io/organizations/miltonbayersandbox"
    recreate_client = true
    user_name       = "timschofield"
    user_key        = "${file("timschofield.pem")}"
  }

}
